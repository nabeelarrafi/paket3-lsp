<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pembelian extends Model
{
    use HasFactory;

    protected $table = 'pembelian';

    protected $fillable = [
        'nonota_beli',
        'tgl_beli',
        'total_beli',
    ];

    // relation one to one user to pembelian
    public function fuser(){
        return $this->belongsTo(User::class, 'id_user', 'id');
    }

    // TODO
    // relation one to many pembelian to detail_pembelian
    public function fdetailPembelian(){
        print_r("todo");
    }

    // relation one to on distibutor to pembelian
    public function fdistributor(){
        return $this->belongsTo(Distributor::class, 'id_user', 'id');
                }

}
